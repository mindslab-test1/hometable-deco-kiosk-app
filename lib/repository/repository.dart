import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/chat_box_controller.dart';
import 'package:htd_app/controllers/video_state_controller.dart';

class Repository {
  static Future<void> sendTextReq(String answer) async {
    final String response =
        await rootBundle.loadString('assets/customized_response.json');

    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    Message message = Message.fromJson(await readJson(answer, response));
    if (message.answer != "") {
      chatBoxController.changeChatInfo(answer, message.answer, message.image,
          message.expectedIntents ?? [], message.webUrl, message.mapUrl);
      if (message.video != "") {
        Get.put(VideoStateController()).changeUrl(message.video,
            changingUrl: ["assets/video/wait_left.mp4"]);
      }
    }
    chatBoxController.updateRenew(true);
  }

  static Future<Map<String, dynamic>> readJson(
      String answer, String response) async {
    var data = json.decode(response);
    return data[answer];
  }
}

class ExpectedIntents {
  final String? intent;
  final String? type;
  final String? url;

  ExpectedIntents({this.intent, this.type, this.url});

  ExpectedIntents.fromJson(Map<String, dynamic> json)
      : intent = json["intent"]?.toString() ?? "",
        type = json["type"]?.toString() ?? "",
        url = json["url"]?.toString() ?? "";
}

class Message {
  final String answer;
  final String image;
  final String video;
  final String webUrl;
  final String mapUrl;
  final List<ExpectedIntents>? expectedIntents;

  Message(
      {this.answer = "",
      this.image = "",
      this.video = "",
      this.expectedIntents,
      this.webUrl = "",
      this.mapUrl = ""});

  factory Message.fromJson(Map<String, dynamic> json) {
    var list = json['expectedIntents'] as List;
    List<ExpectedIntents> eIList =
        list.map((value) => ExpectedIntents.fromJson(value)).toList();

    return Message(
        answer: json['answer']?.toString() ?? "",
        image: json['image']?.toString() ?? "",
        video: json['video']?.toString() ?? "",
        expectedIntents: eIList,
        webUrl: json['webUrl']?.toString() ?? "",
        mapUrl: json['mapUrl']?.toString() ?? "");
  }
}
