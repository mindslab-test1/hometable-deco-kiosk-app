import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/chat_box_controller.dart';
import 'package:htd_app/controllers/port_state_controller.dart';
import 'package:htd_app/repository/repository.dart';

import '../first_button.dart';

class ChoiceMenu extends StatefulWidget {
  const ChoiceMenu({Key? key}) : super(key: key);

  @override
  _ChoiceMenuState createState() => _ChoiceMenuState();
}

class _ChoiceMenuState extends State<ChoiceMenu> {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Container(
      padding: EdgeInsets.only(right: width * 0.03704), // 80
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              SizedBox(
                height: height * 0.2401,
              ),
              SizedBox(
                width: width * 0.40185, // 868
                child: Stack(
                  children: [
                    SizedBox(
                      width: width * 0.37037, // 800037, // 800
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: width * 0.37037, // 800
                            child: Text(
                              '어떠한 안내를 원하시나요?',
                              style: TextStyle(
                                fontFamily: 'notoSansKR',
                                fontWeight: FontWeight.w400,
                                color: const Color(0xff2b2b2d),
                                fontSize: width * 0.02407, // 52
                                letterSpacing: -1.3,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: height * 0.00625, // 24
                          ),
                          SideButton(
                              img: 'assets/img/ico_mug_64px.png',
                              expectedIntents: ExpectedIntents(
                                  intent: '홈·테이블데코페어\n기획관 소개',
                                  type: "button",
                                  url: "")),
                          SideButton(
                            img: 'assets/img/ico_wifi_64px.png',
                            expectedIntents: ExpectedIntents(
                                intent: '온라인 전시관', type: "button", url: ""),
                          ),
                          SideButton(
                            img: 'assets/img/ico_sofa_64px.png',
                            expectedIntents: ExpectedIntents(
                                intent: '디자인 살롱 서울', type: "button", url: ""),
                          ),
                          SideButton(
                            img: 'assets/img/ico_note_64px.png',
                            expectedIntents: ExpectedIntents(
                                intent: 'FAQ', type: "button", url: ""),
                          ),
                          FirstButton(
                            height: height,
                            width: width,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class SideButton extends StatefulWidget {
  final String img;
  final ExpectedIntents expectedIntents;

  const SideButton({Key? key, required this.img, required this.expectedIntents})
      : super(key: key);

  @override
  _SideButtonState createState() => _SideButtonState();
}

class _SideButtonState extends State<SideButton> {
  Color buttonColor = Colors.black.withOpacity(0.4);

  @override
  Widget build(BuildContext context) {
    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return GetBuilder<ChatBoxController>(builder: (controller) {
      return Container(
        width: width * 0.37037, // 800
        margin: EdgeInsets.only(top: height * 0.014167),
        // 40
        child: GestureDetector(
            onTapDown: (TapDownDetails t) {
              setState(() {
                buttonColor = Colors.black.withOpacity(0.7);
              });
            },
            onTapUp: (TapUpDetails tapUpDetails) async {
              Get.put(PortStateController()).isReset = false;

              setState(() {
                buttonColor = Colors.black.withOpacity(0.4);
              });

              if (chatBoxController.isRenewed) {
                chatBoxController.updateRenew(false);
                Repository.sendTextReq(widget.expectedIntents.intent!);
                chatBoxController.updateMenu(false);
              }
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(width * 0.007407),
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: width * 0.00462963,
                  sigmaY: width * 0.00462963,
                ),
                child: Container(
                  padding: EdgeInsets.fromLTRB(
                      width * 0.02963, // 64
                      height * 0.0117875,
                      0,
                      height * 0.0117875),
                  // height: height * 0.04167,
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: width * 0.000462963,
                      color: const Color.fromRGBO(0, 0, 0, 0.25),
                    ),
                    color: buttonColor,
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        children: [
                          SizedBox(
                            height: width * 0.0048,
                          ),
                          SizedBox(
                            width: width * 0.02963,
                            height: width * 0.02963,
                            child: Image.asset(
                              widget.img,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(width: width * 0.02222), // 48
                      Flexible(
                        child: Text(
                          widget.expectedIntents.intent!,
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.visible,
                          style: TextStyle(
                            fontFamily: 'naumSquare',
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: width * 0.02963,
                            height: 1.3,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )),
      );
    });
  }
}
