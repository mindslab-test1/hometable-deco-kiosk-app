import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/chat_box_controller.dart';
import 'package:htd_app/controllers/port_state_controller.dart';
import 'package:htd_app/repository/repository.dart';
import 'package:htd_app/utils/button_util.dart';
import 'package:htd_app/widgets/first_button.dart';
import 'package:htd_app/widgets/schedule_page.dart';

import '../link_page.dart';
import '../map_page.dart';

class ChatMenu extends StatelessWidget {
  const ChatMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Container(
      padding: EdgeInsets.only(right: width * 0.03704), // 80
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(
                  height: height * 0.2401,
                ),
                SizedBox(
                  width: width * 0.40185, // 868
                  child: Stack(
                    children: [
                      SizedBox(
                        width: width * 0.40185, // 868
                        child: GetBuilder<ChatBoxController>(
                          builder: (controller) {
                            List<Widget>? listWidget;
                            /*
                            * chats는 chatbot의 expectedIntents의 intents들
                            * intents를 해당 답변의 버튼들로 보냄
                            * */
                            if (chatBoxController.chats != []) {
                              listWidget = chatBoxController.chats
                                  .map((e) => SideButton(expectedIntents: e))
                                  .toList();
                            } else {
                              listWidget = null;
                            }
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // imageUrl이 없으면 빈 컨테이너 보여줌
                                if (controller.imageUrl != '')
                                  /*
                                    * 이미지마다 다르게 이미지를 보여주기 위함. 해당 개발 당시 img_가 들어간 이미지들이 보여지는 방식이 달랐었음
                                    * */
                                  (controller.imageUrl.contains("img_")
                                      ? Column(
                                          children: [
                                            Container(
                                              width: width * 0.37037, // 800
                                              margin: EdgeInsets.only(
                                                  top: height *
                                                      0.014167), // 40,40
                                              child: Image.asset(
                                                controller.imageUrl,
                                                fit: BoxFit.fitWidth,
                                              ),
                                            ),
                                            SizedBox(
                                              height: height * 0.0125, // 48
                                            ),
                                          ],
                                        )
                                      : Container(
                                          margin: EdgeInsets.only(
                                              top: height * 0.01667), // 64
                                          width: width * 0.37037, // 800
                                          child: (ClipRRect(
                                            borderRadius: BorderRadius.circular(
                                                width * 0.007407), // 16
                                            child: BackdropFilter(
                                              filter: ImageFilter.blur(
                                                sigmaX: width * 0.00833, // 18
                                                sigmaY:
                                                    width * 0.00833, // 00833
                                              ),
                                              child: Center(
                                                child: Container(
                                                  padding: EdgeInsets.all(
                                                      width * 0.02963), // 64
                                                  width: width * 0.39815, // 860
                                                  decoration: BoxDecoration(
                                                    color: Colors.black
                                                        .withOpacity(0.4),
                                                  ),
                                                  child: Center(
                                                      child: SizedBox(
                                                    width: width * 0.33889,
                                                    // 732
                                                    child: Image.asset(
                                                      chatBoxController
                                                          .imageUrl,
                                                      fit: BoxFit.contain,
                                                    ),
                                                  )),
                                                ),
                                              ),
                                            ),
                                          )),
                                        )),
                                SizedBox(
                                  width: width * 0.37037, // 800
                                  child: Text(
                                    chatBoxController.chatTitle,
                                    style: TextStyle(
                                      fontFamily: 'notoSansKR',
                                      fontWeight: FontWeight.w400,
                                      color: const Color(0xff2b2b2d),
                                      fontSize: width * 0.02407,
                                      // 52
                                      letterSpacing: -1.3,
                                    ),
                                  ),
                                ),
                                if (listWidget != null)
                                  Column(
                                    children: [
                                      Wrap(children: listWidget),
                                    ],
                                  ),
                                if (controller
                                    .isBrand) //만약 현재 상태가 brand 버튼인 경우 지도버튼 보여줌
                                  GestureDetector(
                                      onTapDown: (TapDownDetails t) {
                                        // setState(() { });
                                      },
                                      onTapUp:
                                          (TapUpDetails tapUpDetails) async {
                                        // setState(() { });
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => MapPage(
                                                    mapUrl: controller.mapUrl,
                                                  )),
                                        );
                                      },
                                      child: Container(
                                          width: width * 0.37037, // 800
                                          margin: EdgeInsets.only(
                                              top: width * 0.02963),
                                          padding: EdgeInsets.fromLTRB(
                                              width * 0.02963, // 64
                                              height * 0.0117875,
                                              width * 0.02963,
                                              height * 0.0117875),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(
                                                width * 0.007407),
                                            border: Border.all(
                                              width: width * 0.000462963,
                                              color: Colors.black,
                                            ),
                                            color: Colors.white.withOpacity(0),
                                          ),
                                          child: Column(
                                            children: [
                                              Row(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Column(
                                                    children: [
                                                      SizedBox(
                                                        height: width * 0.0036,
                                                      ),
                                                      SizedBox(
                                                        width: width *
                                                            0.02963, // 64
                                                        height: width * 0.02963,
                                                        child: Image.asset(
                                                          'assets/img/ico_gps_64px.png',
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                      width: width *
                                                          0.02222), // 48
                                                  Text(
                                                    '위치안내도',
                                                    textAlign: TextAlign.left,
                                                    overflow:
                                                        TextOverflow.visible,
                                                    style: TextStyle(
                                                      fontFamily: 'naumSquare',
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      color: const Color(
                                                          0xff2b2b2d),
                                                      fontSize:
                                                          width * 0.02963, // 64
                                                      height: 1.2,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ))),
                                if (chatBoxController.webUrl != "")
                                  WebLinkButton(
                                      expectedIntents: ExpectedIntents(
                                          intent:
                                              chatBoxController.currentIntent,
                                          type: "button",
                                          url: ""),
                                      link: chatBoxController.webUrl),
                                SizedBox(
                                  width: width * 0.37037, // 800
                                  child: FirstButton(
                                    height: height,
                                    width: width,
                                  ),
                                )
                              ],
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class SideButton extends StatefulWidget {
  final ExpectedIntents expectedIntents;

  const SideButton({Key? key, required this.expectedIntents}) : super(key: key);

  @override
  _SideButtonState createState() => _SideButtonState();
}

class _SideButtonState extends State<SideButton> {
  Color buttonColor = Colors.black.withOpacity(0.4);

  final brandList = ["TREND FEATURE", "0% WASTE", "BRAND-NEW", "NEXT CREATORS"];

  @override
  Widget build(BuildContext context) {
    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return GetBuilder<ChatBoxController>(builder: (controller) {
      return Container(
        width: width * 0.37037, // 800
        margin: EdgeInsets.only(
            top: height * 0.014167, right: width * 0.01852), // 40, 40
        child: GestureDetector(
          onTapCancel: () {
            setState(() {
              buttonColor = Colors.black.withOpacity(0.4);
            });
          },
          onTapDown: (TapDownDetails t) {
            setState(() {
              buttonColor = Colors.black.withOpacity(0.7);
            });
          },
          onTapUp: (TapUpDetails tapUpDetails) async {
            Get.put(PortStateController()).isReset = false;

            setState(() {
              buttonColor = Colors.black.withOpacity(0.4);
            });

            // brand면 지도 버튼 나오게 해주기 위함
            if (brandList.contains(widget.expectedIntents.intent!)) {
              chatBoxController.updateBrand(true);
            }

            /*
            * isRenew는 버튼 중복클릭을 방지하기 위함
            * 만약 isRenew가 false라면 버튼을 눌러도 아무 동작안함
            * isRenew가 true라면 누르면 isRenew를 false로 바꾸고 chatbot에 질문을 보내 챗봇에서 return이 돌아올때 true로 바꿔줌
            * */
            if (chatBoxController.isRenewed) {
              /*
              * type이 이미지면 버튼을 눌러도 chatbot에 아무것도 보내지 않고 해당하는 이미지를 보여줌
              * */
              if (widget.expectedIntents.type == "image") {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          SchedulePage(url: widget.expectedIntents.url!)),
                );
              } else {
                chatBoxController.updateRenew(false);
                Repository.sendTextReq(widget.expectedIntents.intent!);
              }
            }
          },
          child: ClipRRect(
            borderRadius: BorderRadius.circular(width * 0.007407),
            child: BackdropFilter(
              filter: ImageFilter.blur(
                sigmaX: width * 0.00462963,
                sigmaY: width * 0.00462963,
              ),
              child: Container(
                padding: EdgeInsets.fromLTRB(
                    width * 0.02963, // 64
                    height * 0.0117875,
                    0,
                    height * 0.0117875),
                // height: height * 0.04167,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: width * 0.000462963,
                    color: const Color.fromRGBO(0, 0, 0, 0.25),
                  ),
                  color: buttonColor,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: width * 0.02963,
                      height: width * 0.02963,
                      child: Image.asset(
                        ButtonUtils.getButtonTypes(
                            widget.expectedIntents.intent!),
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(width: width * 0.02222), // 48
                    Flexible(
                      child: Text(
                        widget.expectedIntents.intent!,
                        // softWrap: true,
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.visible,
                        // overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontFamily: 'naumSquare',
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                          fontSize: width * 0.02963,
                          height: 1.1,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    });
  }
}

class WebLinkButton extends StatefulWidget {
  final String link;
  final ExpectedIntents expectedIntents;

  const WebLinkButton(
      {Key? key, required this.expectedIntents, required this.link})
      : super(key: key);

  @override
  _WebLinkButtonState createState() => _WebLinkButtonState();
}

class _WebLinkButtonState extends State<WebLinkButton> {
  Color buttonColor = Colors.black.withOpacity(0);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Container(
      width: width * 0.37037, // 800
      margin: EdgeInsets.only(top: height * 0.014167), // 40
      child: GestureDetector(
        onTapDown: (TapDownDetails t) {
          setState(() {
            buttonColor = Colors.black.withOpacity(0);
          });
        },
        onTapUp: (TapUpDetails tapUpDetails) async {
          setState(() {
            buttonColor = Colors.black.withOpacity(0);
          });
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => LinkPage(linkUrl: widget.link)),
          );
        },
        child: Container(
            padding: EdgeInsets.fromLTRB(
                width * 0.02963, // 64
                height * 0.0117875,
                width * 0.02963,
                height * 0.0117875),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(width * 0.007407),
              border: Border.all(
                width: width * 0.000462963,
                color: Colors.black,
              ),
              color: buttonColor,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.expectedIntents.intent!,
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                        fontFamily: 'naumSquare',
                        fontWeight: FontWeight.w300,
                        // color: Color(0xff2b2b2d),
                        color: const Color(0xff2b2b2d),
                        fontSize: width * 0.01944, // 42
                        height: 1.3,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '바로가기',
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                        fontFamily: 'naumSquare',
                        fontWeight: FontWeight.w400,
                        color: const Color(0xff2b2b2d),
                        fontSize: width * 0.02407, // 52
                        height: 1.35,
                      ),
                    ),
                    SizedBox(
                      width: width * 0.0287037, // 64
                      height: width * 0.0287037, // 64
                      child: Image.asset(
                        'assets/img/ico_arrow_right_64px.png',
                        color: const Color(0xff2b2b2d),
                      ),
                    ),
                  ],
                ),
              ],
            )),
      ),
    );
  }
}
