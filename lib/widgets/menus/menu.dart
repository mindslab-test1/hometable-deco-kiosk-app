import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/chat_box_controller.dart';
import 'package:htd_app/widgets/menus/chat_menu.dart';
import 'package:htd_app/widgets/menus/choice_menu.dart';

class MainMenu extends StatefulWidget {
  const MainMenu({Key? key}) : super(key: key);

  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  @override
  Widget build(BuildContext context) {
    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    return GetBuilder<ChatBoxController>(builder: (_) {
      return chatBoxController.isChatMenu
          ? const ChoiceMenu()
          : const ChatMenu();
    });
  }
}
