import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/video_state_controller.dart';

class BackgroundStoppedImage extends StatelessWidget {
  const BackgroundStoppedImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GetBuilder<VideoStateController>(
        init: VideoStateController(),
        builder: (controller) {
          return SizedBox(
            height: size.height,
            width: size.width,
            child: Image.asset(
              controller.isCenterUrl()
                  ? 'assets/img/background_center.jpg'
                  : 'assets/img/background_left.jpg',
              fit: BoxFit.fill,
            ),
          );
        });
  }
}
