import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/video_state_controller.dart';
import 'package:video_player/video_player.dart';

class AvatarVideoPlayer extends StatefulWidget {
  const AvatarVideoPlayer({Key? key}) : super(key: key);

  @override
  AvatarVideoPlayerState createState() => AvatarVideoPlayerState();
}

class AvatarVideoPlayerState extends State<AvatarVideoPlayer> {
  @override
  void initState() {
    Get.put(VideoStateController()).changeUrl("assets/video/hello.mp4",
        changingUrl: ["assets/video/wait_left.mp4"]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GetBuilder<VideoStateController>(
      init: VideoStateController(),
      builder: (controller) {
        return SizedBox(
            width: size.width * 2.8,
            height: size.height,
            child: controller.controller == null
                ? Container()
                : VideoPlayer(controller.controller as VideoPlayerController));
      },
    );
  }
}
