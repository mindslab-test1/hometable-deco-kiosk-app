import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/port_state_controller.dart';

class MapPage extends StatefulWidget {
  final String mapUrl;
  const MapPage({Key? key, this.mapUrl = ""}) : super(key: key);

  @override
  _MapPage createState() => _MapPage();
}

class _MapPage extends State<MapPage> {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return GetBuilder<PortStateController>(
      builder: (builder) {
        if (builder.isReset) {
          Navigator.pop(context);
        }
        return Scaffold(
          body: Stack(
            children: [
              Container(
                width: width,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/img/kiosk-background.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Positioned(
                top: height * 0.24583, // 944
                right: width * 0.05556, // 120
                child: SizedBox(
                  child: Image.asset(
                    widget.mapUrl,
                    width: width * 0.8889, //1920
                  ),
                ),
              ),
              Positioned(
                top: height * 0.195052083, // 800
                right: width * 0.05556, // 120
                child: SizedBox(
                  width: width * 0.0444, // 96
                  height: width * 0.0444, // 96
                  child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: SizedBox(
                        child: SvgPicture.asset(
                          "assets/img/ico_close_96px.svg",
                          width: width * 0.0444,
                          height: width * 0.0444,
                        ),
                      )),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
