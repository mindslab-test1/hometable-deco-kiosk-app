import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/chat_box_controller.dart';
import 'package:htd_app/controllers/video_state_controller.dart';

class FirstButton extends StatelessWidget {
  const FirstButton({
    Key? key,
    required this.height,
    required this.width,
  }) : super(key: key);

  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: height * 0.02083),
        Center(
          child: GestureDetector(
            onTapUp: (TapUpDetails tap) {
              Get.put(ChatBoxController()).updateMenu(true);
              Get.put(ChatBoxController()).updateBrand(false);
              Get.put(VideoStateController()).changeUrl("assets/video/018.mp4",
                  changingUrl: ["assets/video/wait_left.mp4"]);
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(width * 0.02593),
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 0.0,
                  sigmaY: 0.0,
                ),
                child: Container(
                  padding: EdgeInsets.fromLTRB(width * 0.02963, height * 0.0125,
                      width * 0.02963, height * 0.0125),
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(width * 0.02593),
                        child: BackdropFilter(
                          filter: ImageFilter.blur(
                            sigmaX: width * 0.00462963,
                            sigmaY: width * 0.00462963,
                          ),
                          child: Center(
                            child: Container(
                              width: width * 0.05185,
                              height: width * 0.05185,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                  width: width * 0.000462963,
                                  color:
                                      const Color.fromRGBO(255, 255, 255, 0.8),
                                ),
                                color: Colors.white.withOpacity(0.1),
                                boxShadow: const [
                                  BoxShadow(
                                    color: Colors.black,
                                    spreadRadius: -12.0,
                                    blurRadius: 28.0,
                                  ),
                                ],
                              ),
                              child: Center(
                                child: SizedBox(
                                  width: width * 0.02963,
                                  child: SvgPicture.asset(
                                    "assets/img/ico_home_64px.svg",
                                    color: Colors.white,
                                    fit: BoxFit.fitWidth,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: width * 0.0111),
                      Text(
                        '처음으로',
                        style: TextStyle(
                          fontFamily: 'notoSansKR',
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                          fontSize: width * 0.02593,
                          shadows: [
                            Shadow(
                              blurRadius: width * 0.01852, // 40
                              color: Colors.black.withOpacity(0.4),
                              offset: const Offset(0.0, 0.0),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
