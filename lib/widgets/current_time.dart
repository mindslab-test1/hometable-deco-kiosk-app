import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:timer_builder/timer_builder.dart';

class CurrentTime extends StatefulWidget {
  const CurrentTime({key}) : super(key: key);

  @override
  _CurrentTimeState createState() => _CurrentTimeState();
}

class _CurrentTimeState extends State<CurrentTime> {
  String weekNameEngToKor(String eng) {
    if (eng == 'Monday') {
      return '월';
    } else if (eng == 'Tuesday') {
      return '화';
    } else if (eng == 'Wednesday') {
      return '수';
    } else if (eng == 'Thursday') {
      return '목';
    } else if (eng == 'Friday') {
      return '금';
    } else if (eng == 'Saturday') {
      return '토';
    } else {
      return '일';
    }
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return TimerBuilder.periodic(
      const Duration(seconds: 1),
      builder: (context) {
        return Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                formatDate(DateTime.now(), [
                  mm,
                  '월 ',
                  dd,
                  '일 ${weekNameEngToKor(DateFormat('EEEE').format(DateTime.now()))}요일'
                ]),
                style: TextStyle(
                  color: const Color(0xff2b2b2d),
                  fontSize: width * 0.01481,
                  fontFamily: 'notoSansKR',
                  fontWeight: FontWeight.w400,
                  height: 1,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Column(
                    children: [
                      Text(
                        formatDate(DateTime.now(), [
                          DateFormat('a').format(DateTime.now()) == "PM"
                              ? "오후"
                              : "오전"
                        ]),
                        style: TextStyle(
                          fontFamily: 'notoSansKR',
                          fontWeight: FontWeight.w400,
                          color: const Color(0xff2b2b2d),
                          fontSize: width * 0.01852,
                        ),
                      ),
                      SizedBox(
                        height: height * 0.00364583, // 14
                      ),
                    ],
                  ),
                  SizedBox(width: width * 0.0111),
                  Text(
                    formatDate(DateTime.now(), [hh, ' : ', nn]),
                    style: TextStyle(
                      fontFamily: 'notoSansKR',
                      fontWeight: FontWeight.w500,
                      color: const Color(0xff2b2b2d),
                      fontSize: width * 0.03704,
                    ),
                  ),
                ],
              ),
            ]);
      },
    );
  }
}
