class ButtonUtils {
  static final buttonCoffeeList = [
    "홈·테이블데코페어\n기획관 소개",
    "TREND FEATURE",
    "0% WASTE",
    "BRAND-NEW",
    "NEXT CREATORS"
  ];
  static final buttonWifiList = [
    "온라인 전시관",
    "BRAND LIST",
    "METAVERSE",
    "DECO MAG",
    "ONLINE MALL"
  ];
  static final buttonSofaList = [
    "디자인 살롱 서울",
    "Day 1. Dec. 9th",
    "Day 2. Dec. 10th"
  ];
  static final buttonFAQList = [
    "FAQ",
    "홈·테이블데코페어\n관람 방법은?",
    "홈·테이블데코페어\n관람 시간은?",
    "홈·테이블데코페어는\n재방문이 가능한가요?"
  ];

  // intent에 따라 어떤 버튼 이미지가 보여져야 하는지 결정
  static String getButtonTypes(String intent) {
    if (buttonCoffeeList.contains(intent)) {
      return 'assets/img/ico_mug_64px.png';
    } else if (buttonWifiList.contains(intent)) {
      return 'assets/img/ico_wifi_64px.png';
    } else if (buttonSofaList.contains(intent)) {
      return 'assets/img/ico_sofa_64px.png';
    } else if (buttonFAQList.contains(intent)) {
      return 'assets/img/ico_note_64px.png';
    } else {
      return "";
    }
  }
}
