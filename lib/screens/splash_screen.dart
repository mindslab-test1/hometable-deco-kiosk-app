import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/network_state_controller.dart';
import 'package:htd_app/screens/home_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Get.put(NetworkStateController()).checkNetwork();
  }

  Widget _errorWidget() {
    MediaQueryData mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return ClipRRect(
      borderRadius: BorderRadius.circular(width * 0.007407), // 16
      child: BackdropFilter(
        filter:
            ImageFilter.blur(sigmaX: width * 0.00833, sigmaY: height * 0.00833),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.3),
            borderRadius: BorderRadius.circular(width * 0.007407), // 16
          ),
          height: height * 0.1783854167, // 685
          width: width * 0.3796296, // 820
          child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.0444), // 96
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: height * 0.025, // 96
                  ),
                  Text("UPDATE",
                      style: TextStyle(
                          fontFamily: 'notoSansKR',
                          color: Colors.white,
                          fontSize: width * 0.01852, // 40
                          fontWeight: FontWeight.w700)),
                  SizedBox(
                    height: height * 0.0125, // 48
                  ),
                  Text(
                    "최신버전이 출시되었습니다.\n버전을 업데이트 해주세요.",
                    style: TextStyle(
                        fontFamily: 'notoSansKR',
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                        fontSize: width * 0.02593,
                        // 56
                        letterSpacing: -1.4),
                  ),
                  SizedBox(
                    height: height * 0.01927083, // 74
                  ),
                  Center(
                    child: GestureDetector(
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.circular(width * 0.03981481),
                          color: Colors.deepOrangeAccent,
                          gradient: const RadialGradient(
                            center: Alignment(-1, -0.6),
                            radius: 3.4,
                            colors: [
                              Color(0xFFfd9483),
                              Color(0xFFfd72a6),
                              Color(0xFFb85bfa),
                              Color(0xFF676aff),
                            ],
                          ),
                        ),
                        height: height * 0.03515625, // 135
                        width: width * 0.18056, // 390
                        child: Center(
                          child: Text(
                            "최신 버전 설치",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'notoSansKR',
                                fontSize: width * 0.02222,
                                // 48
                                fontWeight: FontWeight.w700,
                                letterSpacing: -1.2),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Widget _body() {
    MediaQueryData mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return ClipRRect(
      borderRadius: BorderRadius.circular(width * 0.02778),
      child: BackdropFilter(
        filter: ImageFilter.blur(
            sigmaX: MediaQuery.of(context).size.width * 0.00833,
            sigmaY: MediaQuery.of(context).size.height * 0.00833),
        child: Container(
          height: height * 0.030729167, // 118
          width: width * 0.3453704, // 736
          child: Center(
            child: Stack(
              children: [
                Text(
                  "AI Human KIOSK를 시작합니다.",
                  style: TextStyle(
                      fontFamily: 'notoSansKR',
                      fontWeight: FontWeight.w700,
                      fontSize: width * 0.02222,
                      // 48
                      color: Colors.white,
                      letterSpacing: -1),
                ),
              ],
            ),
          ),
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.3), //TODO
            border: Border.all(color: Colors.white),
            borderRadius: BorderRadius.circular(width * 0.02778), // 60
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    Timer(const Duration(seconds: 6),
        () => {Get.off<HomeScreen>(() => const HomeScreen())});
    precacheImage(const AssetImage("assets/img/splashImg.png"), context);

    return Scaffold(
      body: Stack(
        children: [
          SizedBox.expand(
            child: FittedBox(
              child: Image.asset(
                "assets/img/splashImg.png",
              ),
              fit: BoxFit.fill,
            ),
          ),
          Column(
            children: [
              SizedBox(height: height * 0.25729167), // 988
              Center(
                child: SizedBox(
                  height: height * 0.064583, // 248
                  width: width * 0.3333, // 720
                  child: SvgPicture.asset("assets/img/testing.svg"),
                ),
              ),
              GetBuilder<NetworkStateController>(
                  init: NetworkStateController(),
                  builder: (controller) {
                    return Column(children: [
                      SizedBox(
                        height: controller.isError
                            ? (height * 0.03333) // 128
                            : (height * 0.029167), // 112
                      ),
                      // _errorWidget(),
                      _body(),
                    ]);
                  }),
              const Spacer(),
            ],
          ),
        ],
      ),
    );
  }
}
