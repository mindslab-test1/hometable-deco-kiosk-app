import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/device_info_controller.dart';
import 'package:htd_app/widgets/avatar_video_player.dart';
import 'package:htd_app/widgets/background_stopped_image.dart';
import 'package:htd_app/widgets/menus/menu.dart';

import '../controllers/port_state_controller.dart';
import '../widgets/current_time.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Image settingImage;

  @override
  void initState() {
    Get.put(PortStateController()).getPorts();
    Get.put(DeviceInfoController()).onInit();
    settingImage = Image.asset("assets/img/kiosk-background.png");
    // scheduleImage = [Image.asset("assets/img/002.png"),Image.asset("assets/img/003.png"),Image.asset("assets/img/004.png")];
    // allScheduleImage = Image.asset("assets/img/img_timetable01.jpg");
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
    Get.put(PortStateController()).connectTo(null);
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Stack(
            children: [
              const BackgroundStoppedImage(),
              const AvatarVideoPlayer(),
              Positioned(
                top: height * 0.02083, // 80
                left: width * 0.03704, // 80
                child: SizedBox(
                  height: height * 0.049479167, // 190
                  width: width * 0.10185, // 220
                  child: SvgPicture.asset(
                    "assets/img/logo_homeTable.svg",
                    color: const Color(0xff1d1d1b),
                  ),
                ),
              ),
              Positioned(
                right: width * 0.03704,
                top: height * 0.0174479167,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: const [
                    CurrentTime(),
                  ],
                ),
              ),
              GetBuilder<PortStateController>(
                  init: PortStateController(),
                  builder: (controller) => controller.isDetected
                      ? const MainMenu()
                      : Container() // 근접센서 !
                  ),
              // SettingBtn(settingImage: settingImage,),
            ],
          ),
        ),
      ),
    );
  }
}
