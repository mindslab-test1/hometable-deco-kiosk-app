import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/device_info_controller.dart';

class SettingPage extends StatefulWidget {
  final Image settingImage;
  const SettingPage({Key? key, required this.settingImage}) : super(key: key);

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: widget.settingImage.image,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Center(
            child: SizedBox(
                width: width * 0.56667, // 1224
                child: Column(
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: width * 0.56667, // 1224
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              SizedBox(height: height * 0.14583 // 560
                                  ),
                              Center(
                                  child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      SizedBox(
                                        child: SizedBox(
                                          width: width * 0.0444,
                                          height: width * 0.0444,
                                          child: Center(
                                            child: SizedBox(
                                              child: IconButton(
                                                icon: SvgPicture.asset(
                                                  "assets/img/ico_close_64px.svg",
                                                  color: Colors.white,
                                                ),
                                                iconSize: width * 0.02963,
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                      width: width * 0.56667, // 1224
                                      child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                            SizedBox(
                                              width: width * 0.518518, // 1120
                                              child: Column(
                                                children: [
                                                  SizedBox(
                                                    width: width * 0.0444, // 96
                                                    height:
                                                        width * 0.0444, // 96
                                                    child: SvgPicture.asset(
                                                      "assets/img/ico_setting_64px.svg",
                                                      color: const Color(
                                                          0xffA8B0BC),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height:
                                                        height * 0.016667, // 64
                                                  ),
                                                  Text(
                                                    '키오스크 ID',
                                                    style: TextStyle(
                                                      fontFamily: 'notoSansKR',
                                                      fontWeight:
                                                          FontWeight.w700,
                                                      color: Colors.white,
                                                      letterSpacing: -1.4,
                                                      fontSize:
                                                          width * 0.02593, //56
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height:
                                                        height * 0.016667, // 64
                                                  ),
                                                  ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            width *
                                                                0.02178), // 60
                                                    child: BackdropFilter(
                                                        filter:
                                                            ImageFilter.blur(
                                                          sigmaX: width *
                                                              0.00833, // 18
                                                          sigmaY: height *
                                                              0.00833, // 18
                                                        ),
                                                        child: Container(
                                                          decoration:
                                                              BoxDecoration(
                                                            color: Colors.white
                                                                .withOpacity(
                                                                    0.3),
                                                            border: Border.all(
                                                              color:
                                                                  Colors.white,
                                                              width: width *
                                                                  0.000925926, // 2
                                                            ),
                                                            borderRadius: BorderRadius
                                                                .circular(width *
                                                                    0.02178), // 60
                                                          ),
                                                          child: Padding(
                                                              padding: EdgeInsets.fromLTRB(
                                                                  width *
                                                                      0.01481,
                                                                  height *
                                                                      0.0041667,
                                                                  width *
                                                                      0.01481,
                                                                  height *
                                                                      0.0041667),
                                                              child: GetBuilder<
                                                                  DeviceInfoController>(
                                                                init:
                                                                    DeviceInfoController(),
                                                                builder:
                                                                    (controller) {
                                                                  return Text(
                                                                    controller
                                                                        .identifier,
                                                                    style:
                                                                        TextStyle(
                                                                      fontFamily:
                                                                          'notoSansKR',
                                                                      color: Colors
                                                                          .white,
                                                                      fontSize:
                                                                          width *
                                                                              0.01852, // 40
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400,
                                                                      letterSpacing:
                                                                          1.2,
                                                                    ),
                                                                  );
                                                                },
                                                              )),
                                                        )),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            SizedBox(
                                              width: width * 0.003704,
                                            ),
                                          ])),
                                ],
                              )),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                )),
          ),
          /* Positioned(
            child: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
            ),
          ),*/
        ],
      ),
    );
  }
}
