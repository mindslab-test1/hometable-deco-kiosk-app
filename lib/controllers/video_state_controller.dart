import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/port_state_controller.dart';
import 'package:video_player/video_player.dart';

class VideoStateController extends GetxController {
  String _videoUrl = ""; // 현재 실행되고 있는 비디오 url
  VideoPlayerController? _controller; // 현재 실행되고 있는 비디오 컨트롤러
  VideoPlayerController? get controller => _controller;

  String get videoUrl => _videoUrl;

  bool isLoopUrl(String url) {
    // 대기영상의 경우 짧은 영상을 반복해서 보여줘야 하므로 해당 영상이 대기영상인지 아닌지 판별
    return url == "assets/video/wait_center.mp4" ||
        url == "assets/video/wait_left.mp4";
  }

  bool isNetwork(String url) {
    // 만약 asset 동영상이 아닌 network 동영상을 켜야하는 경우 videoplayer를 변환해주기 위하여 설정
    return url.startsWith("http");
  }

  bool isCenterUrl() {
    // 두 영상간 간극을 메워줄 뒷 배경 이미지와 맞추기 위하여 현재 비디오의 인공인간이 가운데있는지 왼쪽에 있는지 구별해주기 위한 값
    return _videoUrl == "assets/video/wait_center.mp4" ||
        _videoUrl == "assets/video/hello.mp4" ||
        _videoUrl == "assets/video/opening.mp4";
  }

  /*
  * 현재 실행되는 비디오 url을 바꾸기 위한 함수
  * url : 바꿀 비디오 url
  * changingUrl : 현재 실행되는 비디오 직후에 실행되어야 할 비디오 url 리스트, 순차적으로 실행
  * warning : 이 함수는 최대한 건드리지 않는 것 추천
  * */
  Future<void> changeUrl(String url,
      {List<String> changingUrl = const []}) async {
    _videoUrl = url;
    // 만약 현재 실행되고 있는 비디오 컨트롤러가 없다면
    if (_controller == null) {
      // network url인지 아닌지에 따라 비디오 플레이어 최초실행
      isNetwork(url)
          ? _initNetworkControllers(url, changingUrl: changingUrl)
          : _initAssetControllers(url, changingUrl: changingUrl);
    } // 만약 현재 실행되고 있는 비디오 컨트롤러가 있다면
    else {
      // 현재 실행되고 있는 컨트롤러를 임시 변수에 저장
      final oldController = _controller;
      // 현재 프레임 직후에 해당 내용 실행
      WidgetsBinding.instance!.addPostFrameCallback((_) async {
        // 현재 실행되고 있던 컨트롤러를 종료
        await oldController!.dispose();
        // 바꿀 url로 실행
        isNetwork(url)
            ? _initNetworkControllers(url, changingUrl: changingUrl)
            : _initAssetControllers(url, changingUrl: changingUrl);
      });
      // 현재 실행되고 있는 컨트롤러 null값으로 초기화
      _controller = null;
      update();
    }
  }

  /*
  * 초기화할 비디오 url이 asset url을 사용하는 경우 초기화 해주기 위한 함수
  * params는 changeUrl과 동일
  * */
  void _initAssetControllers(String url,
      {List<String> changingUrl = const []}) async {
    _videoUrl = url;
    // listener에서 간혹 조건이 2회이상 걸려서 해당 내용이 중복되어 실행될 수 있는데 이를 방지하고자 만든 변수
    AsyncMemoizer finishVideo = AsyncMemoizer<void>();
    // 현재 비디오 컨트롤러를 다음 내용으로 설정
    _controller = VideoPlayerController.asset(url) // 받은 url로 설정
      ..setLooping(isLoopUrl(url)) // 만약 반복해야 하는 url이면 반복 설정
      ..initialize().whenComplete(() {
        // 초기화가 끝나면 실행
        _controller?.play();
        update();
      })
      ..addListener(() {
        // 이 컨트롤러에 listener 설정
        // 만약 현재 controller가 비어있지 않고 바꿀 비디오 url이 있다면
        if (_controller != null && changingUrl.isNotEmpty) {
          // 현재 비디오가 다 끝났다면 (원래는 position과 duration이 같다로 해야하나 listener가 0.001초 단위까지 정확하게잡지 못해서 rough하게 현재 비디오의 재생 새간이 전체 재생 시간에서 0.5초를 뺸것보다 나중에 있다면을 종료 되었다고 간주
          if (_controller!.value.position >=
              _controller!.value.duration - const Duration(milliseconds: 500)) {
            // 해당 listener 조건이 우연히 2회 이상 실행될 여지가 있어 위에서 만들어둔 변수로 한번만 실행하게 설정
            finishVideo.runOnce(() {
              // 맨 처음 동영상이 끝나면 메뉴 보이게 설정
              if (url == "assets/video/hello.mp4") {
                Get.put(PortStateController()).setIsDetected(true);
              }
              if (changingUrl.length == 1) {
                // 만약 현재 바꿀 비디오 url이 1개라면 array의 0번을 실행
                changeUrl(changingUrl[0]);
              } else {
                // 아니라면 현재 array의 0번을 실행하고 바꿀 비디오 url로 현재 바꿀 비디오 url들중 첫번째를 제외한 array 리턴
                changeUrl(changingUrl[0], changingUrl: changingUrl.sublist(1));
              }
            });
          }
        }
      });
  }

  // initAssetControllers와 동일함, videoplayerController가 network를 사용하는 것 하나 다름
  void _initNetworkControllers(String url,
      {List<String> changingUrl = const []}) async {
    _videoUrl = url;

    AsyncMemoizer finishVideo = AsyncMemoizer<void>();
    _controller = VideoPlayerController.network(url)
      ..setLooping(isLoopUrl(url))
      ..initialize().whenComplete(() {
        _controller!.play();
        update();
      })
      ..addListener(() {
        if (_controller != null && changingUrl.isNotEmpty) {
          if (_controller!.value.position >=
              _controller!.value.duration - const Duration(milliseconds: 500)) {
            finishVideo.runOnce(() {
              if (changingUrl.length == 1) {
                changeUrl(changingUrl[0]);
              } else {
                changeUrl(changingUrl[0], changingUrl: changingUrl.sublist(1));
              }
            });
          }
        }
      });
  }
}
