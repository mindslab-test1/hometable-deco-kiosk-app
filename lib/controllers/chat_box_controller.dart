import 'package:get/get.dart';
import 'package:htd_app/repository/repository.dart';

class ChatBoxController extends GetxController {
  List<ExpectedIntents> chats = []; // intents, 현재 답변에 대한 다른 질문 버튼들
  String currentIntent = ""; // 현재 질문이 어떤 intent를 통해서 오게 되었는지 가지고 있는 값
  String chatTitle = ''; // 현재 질문에 대한 답변 text
  String imageUrl = ''; // 현재 질문에 대한 답변에 보여져야 하는 image url
  String webUrl = ''; // 현재 질문에 대한 답변중 webView로 보여져야 하는 화면의 url
  String mapUrl =
      ""; // 현재 질문에 대한 답변중 brand인 경우 지도가 보여져야 하는데 이에 해당하는 지도의 image url
  bool _isRenewed = true; // 버튼 중복 클릭을 방지하기 위해서 debounce해주기 위한 값
  bool isChatMenu =
      true; // 초기 화면의 질문 버튼들과 그 이상의 depth의 버튼들이 design이 달라서 이를 구별하기 위한 값
  bool _isBrand = false; // mapUrl을 사용하기 위한 값

  bool get isRenewed => _isRenewed;
  bool get isBrand => _isBrand;

  void updateBrand(bool isBrand) {
    _isBrand = isBrand;
    update();
  }

  void updateMenu(bool isUpdate) {
    isChatMenu = isUpdate;
    update();
  }

  void updateRenew(bool renew) {
    _isRenewed = renew;
    update();
  }

  void changeChats(List<ExpectedIntents> newChats) {
    chats = newChats;
  }

  // chatbot의 답변 결과를 담기 위한 함수
  void changeChatInfo(String currentChat, String title, String image,
      List<ExpectedIntents> chat, String web, String map) {
    currentIntent = currentChat;
    chatTitle = title;
    imageUrl = image;
    chats = chat;
    webUrl = web;
    mapUrl = map;
    update();
  }

  void resetChatInfo() {
    chats = [];
    currentIntent = "";
    chatTitle = '';
    imageUrl = '';
    webUrl = '';
    mapUrl = "";
    _isBrand = false;
    _isRenewed = true;
    isChatMenu = true;
    update();
  }
}
