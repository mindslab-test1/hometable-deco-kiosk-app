import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/video_state_controller.dart';
import 'package:usb_serial/transaction.dart';
import 'package:usb_serial/usb_serial.dart';

import 'chat_box_controller.dart';

class PortStateController extends GetxController {
  bool _isDetected = false;
  bool isReset = true;

  bool get isDetected => _isDetected;

  UsbPort? _port;
  List<Widget> _ports = [];
  final List<Widget> _serialData = [];

  StreamSubscription<String>? _subscription;
  Transaction<String>? _transaction;
  UsbDevice? _device;

  Future<bool> connectTo(UsbDevice? device) async {
    _serialData.clear();

    if (_subscription != null) {
      _subscription!.cancel();
      _subscription = null;
    }

    if (_transaction != null) {
      _transaction!.dispose();
      _transaction = null;
    }

    if (_port != null) {
      _port!.close();
      _port = null;
    }

    if (device == null) {
      _device = null;
      return true;
    }

    _port = await device.create();
    if (await (_port!.open()) != true) {
      return false;
    }
    _device = device;

    await _port!.setDTR(true);
    await _port!.setRTS(true);
    int totalTime = 0;
    Timer? timer;
    //TODO Change PORT HERE
    await _port!.setPortParameters(
        9600, UsbPort.DATABITS_8, UsbPort.STOPBITS_1, UsbPort.PARITY_NONE);

    _transaction = Transaction.stringTerminated(
        _port!.inputStream as Stream<Uint8List>, Uint8List.fromList([13, 10]));

    _subscription = _transaction!.stream.listen((String line) async {
      // sensor recognized
      // if (line == "yes") {
      //   if (!_isDetected && Get.put(VideoStateController()).videoUrl!='assets/video/opening.mp4') {
      //     Get.put(VideoStateController())
      //         .changeUrl("assets/video/opening.mp4",isChange: true, changingUrl: ["assets/video/018.mp4","assets/video/wait_left.mp4"]);
      //   }
      // }
      if (line == "yes") {
        timer?.cancel();

        // 만약 감지센서에 감지가 되었다면
        /*
        * 만약 직전 상태가 감지센서에 감지되지 않은 상태였고 현재 실행되고 있는 비디오 url이 특정 url이 아닐 때
        * 1. 직전 상태가 감지센서에 감지된 상태인 경우 앞에 서있는 상태이므로 그런경우 무시
        * 2. 직전 상태가 감지센서에 감지되지 않은 상태더라도 현재 실행되고 있던 영상이 인사 영상이라면 인사 영상이 끝나고 다시 실행되도록(인사 영상을 끊지 않도록)
        * */
        if (isReset) {
          Get.put(VideoStateController()).changeUrl("assets/video/018.mp4",
              changingUrl: ["assets/video/wait_left.mp4"]);
          isReset = false;
          update();
        }
      }

      if (line == "no") {
        timer = Timer.periodic(const Duration(milliseconds: 10), (timer) {
          totalTime += 10;
          if (totalTime >= 6000) {
            Get.put(VideoStateController())
                .changeUrl("assets/video/wait_left.mp4");
            Get.put(ChatBoxController()).resetChatInfo();
            isReset = true;
            timer.cancel();
            totalTime = 0;
            update();
          }
        });
      }

      _serialData.add(Text(line));
      if (_serialData.length > 20) {
        _serialData.removeAt(0);
      }
      update();
    });
    return true;
  }

  void getPorts() async {
    _ports = [];
    List<UsbDevice> devices = await UsbSerial.listDevices();
    if (!devices.contains(_device)) {
      connectTo(null);
    }

    for (var device in devices) {
      // * Connect Device if arduino
      if (device.productName == null) {
        connectTo(_device == device ? null : device);
      }

      _ports.add(ListTile(
          leading: const Icon(Icons.usb),
          title: Text(device.productName ?? "null"),
          subtitle: Text(device.manufacturerName ?? 'null'),
          trailing: ElevatedButton(
            child: Text(_device == device ? "Disconnect" : "Connect"),
            onPressed: () {
              connectTo(_device == device ? null : device).then((res) {
                getPorts();
              });
            },
          )));
    }
    update();
  }

  void setIsDetected(bool isDetected) {
    _isDetected = isDetected;
    update();
  }
}
